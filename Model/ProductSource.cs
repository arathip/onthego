using System;
using System.Linq;
using OnTheGo.Models;
using System.Collections.Generic;

namespace OnTheGo.Models
{
	public class ProductSource
	{
		private static ProductSource instance;

		private ProductSource() 
		{
			Product p1 = new Product ("Mango Chutney", 18, "Mango.png");
			Product p2 = new Product ("Apple Pear Chutney", 16, "ApplePear.png");
			Product p3 = new Product ("Apricot Chutney", 20, "Apricot.png");
			Product p4 = new Product ("Apple Caramel", 18, "AppleCaramel.png");
			Product p5 = new Product ("Chilli Jam Chutney", 18, "ChilliJam.png");

			catalogProducts.Add(p1);
			catalogProducts.Add(p2);
			catalogProducts.Add(p3);
			catalogProducts.Add (p4);
			catalogProducts.Add (p5);
		}

		public static ProductSource Instance
		{
			get 
			{
				if (instance == null)
				{
					instance = new ProductSource();
				}
				return instance;
			}
		}

		List<Product> catalogProducts = new List<Product>();

		public List<Product> GetProducts ()
		{
			return catalogProducts;
		}
		
		public void SaveProduct (Product product)
		{
			catalogProducts.Add (product);
		}
	}
}

