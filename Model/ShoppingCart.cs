using System;
using OnTheGo.Models;
using System.Collections.Generic;

namespace OnTheGo.Models
{
	public class ShoppingCart
	{
		private Dictionary<Product, Double> ProductsInCart;
		private static ShoppingCart SingleInstance = new ShoppingCart();
		private Double Discount = 0;
		private ShoppingCart ()
		{
			ProductsInCart = new Dictionary<Product, Double> ();
		}

		public static ShoppingCart GetInstance()
		{
			return SingleInstance;
		}

		public Double QuantityFor (Product currentProduct)
		{
			return ProductsInCart.ContainsKey(currentProduct) ? ProductsInCart[currentProduct] : 0;
		}

		public int ProductCount ()
		{
			return ProductsInCart.Count;
		}

		public Product ProductAt (int row)
		{
			return new List<Product>(this.ProductsInCart.Keys)[row];
		}

		public Double CartTotalAmount()
		{
			Double total = 0;
			foreach(Product aProduct in ProductsInCart.Keys)
			{
				total = total + ProductsInCart [aProduct] * aProduct.Cost;
			}
			total = total - Discount;
			return Math.Round(total,2);
		}

		public void SetDiscountValue(Double discountValue)
		{
			this.Discount = discountValue;
		}

		public void UpdateQuantityForProduct (Product currentProduct, Double quantity)
		{
			if (ProductsInCart.ContainsKey (currentProduct)) {
				if (quantity > 0)
					ProductsInCart [currentProduct] = quantity;
				if (quantity == 0)
					ProductsInCart.Remove (currentProduct);
			} else {
				if (quantity > 0)ProductsInCart [currentProduct] = quantity;
			}
		}
	}
}

