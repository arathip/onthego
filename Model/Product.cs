using System;

namespace OnTheGo.Models

{
	public class Product
	{
		public Product()
		{
		}

		public Product (string name, int cost, string imagePath)
		{
			this.Name = name;
			this.Cost = cost;
			this.uid = System.Guid.NewGuid();
			this.ImagePath = imagePath;
		}

		public Guid uid { get; set; }
		public string Name { get; set; }
		public Double Cost { get;  set; }
		public String ImagePath { get; set; }
		public override bool Equals (object obj)
		{
			var convertedObj = obj as Product;
			if (convertedObj == null)
				return false;

			return this.uid.Equals (convertedObj.uid);
		}

		public override int GetHashCode ()
		{
			return this.uid.GetHashCode();
		}

	}
}

