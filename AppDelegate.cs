using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using OnTheGo.Views;

namespace OnTheGo
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UIWindow window;
		UINavigationController _navController;
		UITableViewController _homeViewController;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			window = new UIWindow (UIScreen.MainScreen.Bounds);
			window.MakeKeyAndVisible ();
			this._navController = new UINavigationController ();
			this._navController.NavigationBar.TintColor = UIColor.FromRGB (0,67,62);
			this._homeViewController = new ProductCatalogListController();
			this._navController.PushViewController(this._homeViewController, false);
			window.RootViewController = this._navController;
			window.MakeKeyAndVisible ();
			
			return true;
		}
	}
}

