// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace OnTheGo
{
	[Register ("MasterProductListViewController")]
	partial class MasterProductListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView imageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField productCost { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField productTitle { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton saveProduct { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton takePhoto { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imageView != null) {
				imageView.Dispose ();
				imageView = null;
			}

			if (takePhoto != null) {
				takePhoto.Dispose ();
				takePhoto = null;
			}

			if (productTitle != null) {
				productTitle.Dispose ();
				productTitle = null;
			}

			if (productCost != null) {
				productCost.Dispose ();
				productCost = null;
			}

			if (saveProduct != null) {
				saveProduct.Dispose ();
				saveProduct = null;
			}
		}
	}
}
