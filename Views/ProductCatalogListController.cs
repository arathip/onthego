using System;
using MonoTouch.UIKit;
using OnTheGo.Models;
using System.Drawing;
using MonoTouch.Foundation;
using System.Collections.Generic;


namespace OnTheGo.Views
{
	public class ProductCatalogListController : UITableViewController
	{
		ProductCatalogTableSource _tableSource = null;
		CartViewController _detailsScreen = null;

		public ProductCatalogListController () : base ()
		{
			this.Initialize ();
			this.Title = "Product Catalog";
		}

		protected void Initialize()
		{
			this.NavigationItem.SetRightBarButtonItem (new UIBarButtonItem ("Next", UIBarButtonItemStyle.Plain, null), true);
			this.NavigationItem.RightBarButtonItem.Clicked += (sender, e) => { this.ShowTaskDetails(); };

			this.NavigationItem.BackBarButtonItem = new UIBarButtonItem ("Back", UIBarButtonItemStyle.Plain, null);
		}
		
		protected void ShowTaskDetails()
		{
			this._detailsScreen = new CartViewController();
			this.NavigationController.PushViewController(this._detailsScreen, true);
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			this.PopulateTable();			
		}
		
		protected void PopulateTable()
		{
			this._tableSource = new ProductCatalogTableSource (ProductSource.Instance.GetProducts ());
			this.TableView.Source = this._tableSource;	
		}		

	}

}

