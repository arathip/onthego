// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace OnTheGo.Views
{
	[Register ("CartViewController")]
	partial class CartViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView CartProductList { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField DiscountValue { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField TotalValue { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CartProductList != null) {
				CartProductList.Dispose ();
				CartProductList = null;
			}

			if (DiscountValue != null) {
				DiscountValue.Dispose ();
				DiscountValue = null;
			}

			if (TotalValue != null) {
				TotalValue.Dispose ();
				TotalValue = null;
			}
		}
	}
}
