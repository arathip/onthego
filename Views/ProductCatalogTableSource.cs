using System;
using MonoTouch.UIKit;
using System.Collections.Generic;
using OnTheGo.Models;
using MonoTouch.Foundation;
using System.Drawing;

namespace OnTheGo.Views
{
	public class ProductCatalogTableSource : UITableViewSource
	{
		protected List<Product> listofProducts = new List<Product>();

		public ProductCatalogTableSource (List<Product> listofProducts) : base ()
		{
			this.listofProducts = listofProducts;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			ProductCatalogCell cell = tableView.DequeueReusableCell (ProductCatalogCell.Key) as ProductCatalogCell;
			if (cell == null)
				cell = new ProductCatalogCell (ProductCatalogCell.Key);
			cell.UpdateCell (listofProducts[indexPath.Row]);
			return cell;
		}
		
		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 70;
		}
		
		public override int RowsInSection (UITableView tableview, int section)
		{
			return this.listofProducts.Count;
		}
		
		public override int NumberOfSections (UITableView tableView)
		{
			return 1;
		}

	}
}

