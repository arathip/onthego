using System;
using MonoTouch.UIKit;
using OnTheGo.Models;
using System.Drawing;
using MonoTouch.Foundation;

namespace OnTheGo
{
	public class CartTableCell : UITableViewCell
	{

		public UILabel Name, Cost, ItemCost;
		public UIImageView ProductImageView;
		private Product CurrentProduct;

		public static readonly NSString Key = new NSString ("CartTableCell");

		public CartTableCell (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
			ProductImageView = new UIImageView();

			Name = new UILabel () {
				Font = UIFont.FromName("Arial", 16f),
				TextColor = UIColor.FromRGB (74, 77, 78),
				BackgroundColor = UIColor.Clear
			};

			Cost = new UILabel () {
				Font = UIFont.FromName("Arial", 14f),
				TextColor = UIColor.FromRGB (174, 174, 174),
				BackgroundColor = UIColor.Clear
			};


			ItemCost = new UILabel () {
				Font = UIFont.FromName("Arial", 22f),
				TextColor = UIColor.FromRGB (0, 0, 0),
				BackgroundColor = UIColor.Clear,
				TextAlignment = UITextAlignment.Center
			};

			ContentView.Add (Name);
			ContentView.Add (Cost);
			ContentView.Add (ProductImageView);
			ContentView.Add (ItemCost);
		}

		public void UpdateCell (Product product, Double quantity)
		{
			ProductImageView.Image = new UIImage(product.ImagePath);
			ProductImageView.Layer.CornerRadius = 5;
			ProductImageView.ClipsToBounds = true;
			Name.Text = product.Name;
			Cost.Text = "$ " + product.Cost.ToString() + " X " + quantity.ToString() ;
			ItemCost.Text = "$ " + (product.Cost * quantity).ToString ();
			CurrentProduct = product;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			ProductImageView.Frame = new RectangleF(10, 10, 50, 50);
			Name.Frame = new RectangleF(80, 10, ContentView.Bounds.Width - 63, 25);
			Cost.Frame = new RectangleF(80, 20, 100, 40);
			ItemCost.Frame = new RectangleF (230, 9, 80, 40);
		}
	}
}

