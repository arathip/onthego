using System;
using MonoTouch.UIKit;
using OnTheGo.Models;
using MonoTouch.Foundation;

namespace OnTheGo
{
	public class CartTableSource : UITableViewSource
	{

		private ShoppingCart UserShoppingCart = ShoppingCart.GetInstance();

		public CartTableSource() : base()
		{

		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			CartTableCell cell = tableView.DequeueReusableCell (CartTableCell.Key) as CartTableCell;
			if (cell == null)
				cell = new CartTableCell(CartTableCell.Key);
			Product currentProduct = UserShoppingCart.ProductAt (indexPath.Row);
			cell.UpdateCell (currentProduct, UserShoppingCart.QuantityFor(currentProduct));
			return cell;
		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 60;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return this.UserShoppingCart.ProductCount();
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 1;
		}
	}
}

