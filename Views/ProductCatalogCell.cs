using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using OnTheGo.Models;
using MonoTouch.CoreGraphics;

namespace OnTheGo.Views
{	
	public partial class ProductCatalogCell : UITableViewCell  {
		public UILabel Name, Cost, Quantity;
		public UIImageView ProductImageView;
		public UIButton IncrementQuantityButton;
		public UIButton DecrementQuantityButton;
		public UIStepper QuantityPicker;
		private ShoppingCart UserShoppingCart = ShoppingCart.GetInstance();
		private Product CurrentProduct;

		public static readonly NSString Key = new NSString ("ProductCatalogCell");

		public ProductCatalogCell (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
			ProductImageView = new UIImageView();

			Name = new UILabel () {
				Font = UIFont.FromName("Arial", 16f),
				TextColor = UIColor.FromRGB (74, 77, 78),
				BackgroundColor = UIColor.Clear
			};

			Cost = new UILabel () {
				Font = UIFont.FromName("Arial", 12f),
				TextColor = UIColor.FromRGB (174, 174, 174),
				TextAlignment = UITextAlignment.Center,

				BackgroundColor = UIColor.Clear
			};

			QuantityPicker = new UIStepper(){
				MinimumValue = 0,
				MaximumValue = 100
			};
			QuantityPicker.TouchUpInside += (object sender, EventArgs e) => UpdateItemQuantity(sender, e);

			Quantity = new UILabel () {
				Font = UIFont.FromName("Arial", 14f),
				TextColor = UIColor.FromRGB (0, 0, 0),
				BackgroundColor = UIColor.Clear,
				TextAlignment = UITextAlignment.Center,
				Text = "0"
			};

			ContentView.Add (Name);
			ContentView.Add (Cost);
			ContentView.Add (ProductImageView);
			ContentView.Add (QuantityPicker);
			ContentView.Add (Quantity);
		}

		private void UpdateItemQuantity (object sender, EventArgs e)
		{
			Quantity.Text = QuantityPicker.Value.ToString();
			UserShoppingCart.UpdateQuantityForProduct (CurrentProduct, QuantityPicker.Value);
		}

		public void UpdateCell (Product product)
		{
			ProductImageView.Image = new UIImage(product.ImagePath);
			ProductImageView.Layer.CornerRadius = 5;
			ProductImageView.ClipsToBounds = true;
			Name.Text = product.Name;
			Cost.Text = "$ " +product.Cost.ToString();
			CurrentProduct = product;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			ProductImageView.Frame = new RectangleF(10, 10, 50, 50);
			Name.Frame = new RectangleF(70, 13, ContentView.Bounds.Width - 63, 25);
			Cost.Frame = new RectangleF(62, 27, 40, 40);
			QuantityPicker.Frame = new RectangleF (220, 30, 40, 20);
			Quantity.Frame = new RectangleF (245, 0, 40,40);
		}
	}
}

