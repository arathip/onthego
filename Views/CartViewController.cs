using MonoTouch.UIKit;
using System.Drawing;
using System;
using MonoTouch.Foundation;
using OnTheGo.Models;

namespace OnTheGo.Views
{
	public partial class CartViewController : UIViewController
	{
		CartTableSource _tableSource;
		private ShoppingCart _userShoppingCart;

		public CartViewController () : base ("CartViewController", null)
		{
			_userShoppingCart = ShoppingCart.GetInstance ();
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();	
			this.Title = "Cart";
			InitializeNavigation ();
			PopulateTable ();
			this.DiscountValue.EditingChanged += (object sender, EventArgs e) => DiscountValueEdited(sender, e);
			RegisterKeyboardVisibilityHandler ();
			RegisterForKeyboardNotification ();
		}

		private void DiscountValueEdited (object sender, EventArgs e)
		{
			Double discount = string.IsNullOrEmpty (DiscountValue.Text) ? 0 : Double.Parse (DiscountValue.Text);
			_userShoppingCart.SetDiscountValue (discount);
			TotalValue.Text = _userShoppingCart.CartTotalAmount ().ToString();
		}

		public void InitializeNavigation()
		{
			this.NavigationItem.SetRightBarButtonItem (new UIBarButtonItem ("Pay", UIBarButtonItemStyle.Plain, null), true);
			this.NavigationItem.RightBarButtonItem.Clicked += (sender, e) => { this.ShowConfirmationScreen(); };
		}

		private void ShowConfirmationScreen()
		{

		}

		protected void PopulateTable()
		{
			if(_userShoppingCart.ProductCount() == 0) this.CartProductList.Hidden = true;
			this._tableSource = new CartTableSource ();
			this.CartProductList.Source = this._tableSource;
			this.DiscountValue.Text = "0";
			this.TotalValue.Text = _userShoppingCart.CartTotalAmount ().ToString();
		}

		public void RegisterKeyboardVisibilityHandler() 
		{
			UITapGestureRecognizer tap = new UITapGestureRecognizer (DismissKeyboard);
			this.View.AddGestureRecognizer (tap);
		}

		private void DismissKeyboard()
		{
			this.DiscountValue.ResignFirstResponder ();
		}

		private void RegisterForKeyboardNotification()
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardWillBeShown);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardWillBeHidden);
		}

		private void KeyboardWillBeShown(NSNotification notification)
		{

			var keyboardBounds = (NSValue)notification.UserInfo.ObjectForKey(UIKeyboard.BoundsUserInfoKey);
			var keyboardSize = keyboardBounds.RectangleFValue;
			ViewMovedUp (true, keyboardSize);
		}

		private void ViewMovedUp (bool movedUp, RectangleF keyboardSize)
		{
			UIView.BeginAnimations ("MoveUp");
			UIView.SetAnimationDuration (0.3);
			float offsetForKeyboard = keyboardSize.Height;
			RectangleF frame = this.View.Frame;
			if (movedUp) {
				frame.Y -= offsetForKeyboard;
				frame.Height += offsetForKeyboard;
			}
			else {
				frame.Y += offsetForKeyboard;
				frame.Height -= offsetForKeyboard;
			}
			this.View.Frame = frame;
			UIView.CommitAnimations ();
		}

		private void KeyboardWillBeHidden(NSNotification notification)
		{
			var keyboardBounds = (NSValue)notification.UserInfo.ObjectForKey(UIKeyboard.BoundsUserInfoKey);
			var keyboardSize = keyboardBounds.RectangleFValue;
			ViewMovedUp (false, keyboardSize);

		}
	}
}

