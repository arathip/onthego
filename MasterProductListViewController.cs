using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.IO;
using OnTheGo.Models;

namespace OnTheGo
{
	public partial class MasterProductListViewController : UIViewController
	{
		private UIImagePickerController imagePicker;
		private Product _currentProduct;

		public MasterProductListViewController () : base ("MasterProductListViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			InitializeController ();
		}

		void InitializeController ()
		{
			this.takePhoto.TouchUpInside += (sender, e) => { TakePhoto(); };
			this.saveProduct.TouchUpInside += (sender, e) => { SaveProduct(); };
			RegisterForKeyboardNotification ();
			RegisterKeyboardVisibilityHandler ();
			_currentProduct = new Product ();
		}

		private void SaveProduct ()
		{
			_currentProduct.Cost = Double.Parse(this.productCost.Text);
			_currentProduct.Name = this.productTitle.Text.ToString ();
		}

		private void TakePhoto ()
		{
			imagePicker = new UIImagePickerController ();
			imagePicker.AllowsEditing = true;
			imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
			imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes (UIImagePickerControllerSourceType.PhotoLibrary);
			imagePicker.FinishedPickingMedia += Handle_FinishedPickingMedia;
			imagePicker.Canceled += Handle_Canceled;

			NavigationController.PresentViewController(imagePicker, true, null);
		}

		void Handle_FinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
		{
			bool isImage = false;
			switch(e.Info[UIImagePickerController.MediaType].ToString()) {
				case "public.image":
					isImage = true;
					break;
				case "public.video":
					break;
			}

			// get common info (shared between images and video)
			NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;
			if (referenceURL != null)
				Console.WriteLine("Url:"+referenceURL.ToString ());

			// if it was an image, get the other image info
			if(isImage) {
				// get the original image
				UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
				if(originalImage != null) {
					// do something with the image
					imageView.Image = originalImage; // display
					SaveImageInDocumentDirectory (originalImage);
				}
			} 

			// dismiss the picker
			imagePicker.DismissViewController (true, null);
		}

		void SaveImageInDocumentDirectory (UIImage originalImage)
		{
			string name = System.Guid.NewGuid ().ToString();
			NSData pngData = originalImage.AsPNG ();
			var documents = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			var fileName = Path.Combine (documents, name);
			NSError err = null;
			pngData.Save (fileName, false, out err);
			_currentProduct.ImagePath = fileName;
		}

		void Handle_Canceled (object sender, EventArgs e)
		{
			imagePicker.DismissViewController (true, null);
		}

		public void RegisterKeyboardVisibilityHandler() 
		{
			UITapGestureRecognizer tap = new UITapGestureRecognizer (DismissKeyboard);
			this.View.AddGestureRecognizer (tap);
		}

		private void DismissKeyboard()
		{
			this.productCost.ResignFirstResponder ();
			this.productTitle.ResignFirstResponder ();
		}

		private void RegisterForKeyboardNotification()
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardWillBeShown);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardWillBeHidden);
		}

		private void KeyboardWillBeShown(NSNotification notification)
		{

			var keyboardBounds = (NSValue)notification.UserInfo.ObjectForKey(UIKeyboard.BoundsUserInfoKey);
			var keyboardSize = keyboardBounds.RectangleFValue;
			ViewMovedUp (true, keyboardSize);
		}

		private void ViewMovedUp (bool movedUp, RectangleF keyboardSize)
		{
			UIView.BeginAnimations ("MoveUp");
			UIView.SetAnimationDuration (0.3);
			float offsetForKeyboard = keyboardSize.Height;
			RectangleF frame = this.View.Frame;
			if (movedUp) {
				frame.Y -= offsetForKeyboard;
				frame.Height += offsetForKeyboard;
			}
			else {
				frame.Y += offsetForKeyboard;
				frame.Height -= offsetForKeyboard;
			}
			this.View.Frame = frame;
			UIView.CommitAnimations ();
		}

		private void KeyboardWillBeHidden(NSNotification notification)
		{
			var keyboardBounds = (NSValue)notification.UserInfo.ObjectForKey(UIKeyboard.BoundsUserInfoKey);
			var keyboardSize = keyboardBounds.RectangleFValue;
			ViewMovedUp (false, keyboardSize);

		}
	}
}

